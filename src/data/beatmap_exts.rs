use osu_db::{listing::Beatmap, Replay};

pub trait ReplayExt {
    fn accuracy(&self) -> f32;
}

impl ReplayExt for Replay {
    fn accuracy(&self) -> f32 {
        let (c300, c100, c50, cmiss) = (
            self.count_300 as f32,
            self.count_100 as f32,
            self.count_50 as f32,
            self.count_miss as f32,
        );

        let all = c300 + c100 + c50 + cmiss;
        (c300 * 300.0 + c100 * 100.0 + c50 * 50.0) / (all * 300.0)
    }
}

pub trait BeatmapExt {
    fn title(&self) -> String;
    fn cover_url(&self) -> String;
    fn url(&self) -> String;
}

impl BeatmapExt for Beatmap {
    fn title(&self) -> String {
        format!(
            "{} - {} [{}]",
            self.artist_ascii.as_deref().unwrap_or_default(),
            self.title_ascii.as_deref().unwrap_or_default(),
            self.difficulty_name.as_deref().unwrap_or_default(),
        )
    }

    fn cover_url(&self) -> String {
        format!("https://assets.ppy.sh/beatmaps/{}/covers/list.jpg", self.beatmapset_id)
    }

    fn url(&self) -> String {
        format!("https://osu.ppy.sh/beatmapsets/{}#osu/{}", self.beatmapset_id, self.beatmap_id)
    }
}
