pub mod recent;
pub mod beatmap_exts;

use log::info;
use chrono::{offset::Utc, DateTime};
use osu_db::{listing::Beatmap, replay::Replay, score::BeatmapScores, Listing, ScoreList};
use std::cmp::{Eq, Ord, Ordering, PartialEq, PartialOrd, Reverse};
use std::collections::{BinaryHeap, HashMap};
use std::fs;
use std::path::Path;
use std::time::SystemTime;
use thiserror::Error;

struct Timestamped<'a> {
    last_played: DateTime<Utc>,
    hash: &'a String,
    scores: &'a Vec<Replay>,
}

impl<'a> PartialEq for Timestamped<'a> {
    fn eq(&self, other: &Timestamped<'_>) -> bool {
        self.last_played.eq(&other.last_played)
    }
}

impl<'a> Eq for Timestamped<'a> {}

impl<'a> PartialOrd for Timestamped<'a> {
    fn partial_cmp(&self, other: &Timestamped<'_>) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

impl<'a> Ord for Timestamped<'a> {
    fn cmp(&self, other: &Timestamped<'_>) -> Ordering {
        Reverse(self.last_played).cmp(&Reverse(other.last_played))
    }
}

#[derive(Error, Debug, Clone)]
pub enum DataError {
    #[error("While reading osudb: {0}")]
    OsuDb(String),

    #[error("osu!.db was not found in the osu home directory")]
    OsuDbNotFound,

    #[error("scores.db was not found in the osu home directory")]
    ScoresDbNotFound,
}

/// I have to use `.to_string()` here because otherwise DataError is not Clone,
/// which makes some things harder/impossible on iced :/
impl From<osu_db::Error> for DataError {
    fn from(e: osu_db::Error) -> Self {
        Self::OsuDb(e.to_string())
    }
}

#[derive(Debug, Clone)]
pub struct Data {
    pub beatmaps: HashMap<String, Beatmap>,
    pub scores: HashMap<String, Vec<Replay>>,
    modified: SystemTime,
}

impl Data {
    pub async fn from_dir<P: AsRef<Path>>(dir: P) -> Result<Self, DataError> {
        info!("Reading databases from {:?}", dir.as_ref());

        if !dir.as_ref().join("osu!.db").exists() {
            return Err(DataError::OsuDbNotFound);
        }
        if !dir.as_ref().join("scores.db").exists() {
            return Err(DataError::ScoresDbNotFound);
        }

        let osu_db_path = dir.as_ref().join("osu!.db");
        let scores_db_path = dir.as_ref().join("scores.db");

        use std::time::Instant;
        let tic = Instant::now();
        let modified = last_modified(dir);

        // Read ScoreList and transform to Map<hash, Vec<Replay>>
        let scores = ScoreList::from_file(scores_db_path)?
            .beatmaps
            .into_iter()
            .map(|b| (b.hash.clone().unwrap_or_default(), b.scores))
            .collect();

        // Read beatmap listing and transform to Map<hash, Beatmap>
        let listing = Listing::from_file(osu_db_path)?;
        let beatmaps = listing
            .beatmaps
            .into_iter()
            .map(|bs| (bs.hash.clone().unwrap_or_default(), bs))
            .collect();

        let duration = tic.elapsed();
        log::info!("Took {:?}", duration);

        Ok(Self {
            beatmaps,
            scores,
            modified,
        })
    }

    pub fn outdated<P: AsRef<Path>>(&self, dir: P) -> bool {
        last_modified(dir) > self.modified
    }

    pub fn most_recent_maps(&self, n: usize) -> Vec<BeatmapScores> {
        let mut heap = BinaryHeap::new();
        for (hash, scores) in &self.scores {
            let last_played = scores
                .iter()
                .map(|replay| replay.timestamp)
                .max()
                .unwrap_or(DateTime::<Utc>::MIN_UTC);

            heap.push(Timestamped {
                last_played,
                hash,
                scores,
            });
            if heap.len() > n {
                heap.pop();
            }
        }

        heap.into_sorted_vec()
            .into_iter()
            .map(|t| BeatmapScores {
                hash: Some(t.hash.clone()),
                scores: t.scores.clone(),
            })
            .collect()
    }
}

/// When were the relevant files in `dir` last modified?
/// Current relevant files are osu!.db and scores.db
fn last_modified<P: AsRef<Path>>(dir: P) -> SystemTime {
    let osu_time = fs::metadata(dir.as_ref().join("osu!.db"))
        .ok()
        .and_then(|m| m.modified().ok())
        .unwrap_or(SystemTime::now());

    let scores_time = fs::metadata(dir.as_ref().join("scores.db"))
        .ok()
        .and_then(|m| m.modified().ok())
        .unwrap_or(SystemTime::now());

    osu_time.max(scores_time)
}
