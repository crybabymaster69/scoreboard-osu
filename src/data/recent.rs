use osu_db::{listing::Beatmap, replay::Replay};

use super::Data;

#[derive(Debug)]
pub struct RecentPlay {
    pub beatmap: Beatmap,
    pub replays: Vec<Replay>,
}

pub fn recent_plays(data: &Data) -> Vec<RecentPlay> {
    let mut res = vec![];

    for beatmap in data.most_recent_maps(25) {
        // get the last 4 replays, ordered from oldest to newest
        // code is kind of hard to read sorry
        let replays: Vec<Replay> = {
            use std::cmp::Reverse;
            let mut replays = beatmap.scores;
            replays.sort_unstable_by(|r0, r1| Reverse(r0.timestamp).cmp(&Reverse(r1.timestamp)));
            let first_4_replays = &replays[0..replays.len().min(4)];
            let mut first_4_replays: Vec<_> = first_4_replays.to_vec();
            first_4_replays.reverse();
            first_4_replays
        };

        let beatmap = match data
            .beatmaps
            .get(beatmap.hash.as_deref().unwrap_or_default())
        {
            Some(x) => x.clone(),
            None => continue,
        };

        res.push(RecentPlay { beatmap, replays });
    }

    res
}

