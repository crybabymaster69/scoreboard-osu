pub mod ui;

use color_eyre::Result;
use iced::{window, Application, Settings};
use scoreboard_osu::config::Config;
use simple_logger::SimpleLogger;
use ui::Ui;

fn main() -> Result<()> {
    color_eyre::install()?;
    SimpleLogger::new()
        .with_level(log::LevelFilter::Off)
        .with_module_level("scoreboard_osu", log::LevelFilter::Trace)
        .env()
        .init()
        .unwrap();

    Config::global().load();
    Ok(Ui::run(Settings {
        window: window::Settings {
            size: (768, 768),
            ..window::Settings::default()
        },
        ..Settings::default()
    })?)
}
