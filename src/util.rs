pub fn put_dots_in_the_middle_of_a_number(mut x: u32) -> String {
    let mut s = String::new();
    let mut i = 0;
    while x > 0 {
        s.push((x % 10 + '0' as u32) as u8 as char);
        x /= 10;

        if i % 3 == 2 && x > 0 {
            s.push('.');
        }
        i += 1;
    }
    s.chars().rev().collect()
}
