use iced::theme;
use iced::widget::container;

pub fn score_theme(score: u32) -> theme::Text {
    let c = iced::Color::from_rgb8;

    theme::Text::Color(match score {
        0..=99999 => c(214, 39, 68),
        100000..=299999 => c(227, 113, 6),
        300000..=499999 => c(230, 194, 24),
        500000..=599999 => c(146, 219, 52),
        600000..=799999 => c(14, 193, 65),
        800000..=1199999 => c(12, 125, 221),
        1200000.. => c(131, 131, 131),
    })
}

pub struct TooltipStyle;

impl From<TooltipStyle> for iced::theme::Container {
    fn from(ts: TooltipStyle) -> Self {
        Self::Custom(Box::new(ts))
    }
}

impl container::StyleSheet for TooltipStyle {
    type Style = iced::Theme;

    fn appearance(&self, _style: &Self::Style) -> container::Appearance {
        container::Appearance {
            background: Some(iced::Background::Color(iced::Color::from_rgb8(10, 10, 10))),
            border_radius: 10.0.into(),
            ..container::Appearance::default()
        }
    }
}

pub struct HeaderStyle;

impl From<HeaderStyle> for iced::theme::Container {
    fn from(ts: HeaderStyle) -> Self {
        Self::Custom(Box::new(ts))
    }
}

impl container::StyleSheet for HeaderStyle {
    type Style = iced::Theme;

    fn appearance(&self, _style: &Self::Style) -> container::Appearance {
        container::Appearance {
            background: Some(iced::Background::Color(iced::Color::from_rgb8(20, 20, 20))),
            ..container::Appearance::default()
        }
    }
}
