use rfd::AsyncFileDialog;

pub async fn folder() -> Option<String> {
    let handle = AsyncFileDialog::new()
        .pick_folder()
        .await?;

    let path = handle.path();
    let path = path.to_str()?;
    Some(path.to_string())
}
