use iced::widget::image::Handle;
use std::{borrow::Cow, fmt};

#[derive(Default, Clone)]
pub struct Image {
    pub data: Vec<u8>,
}

impl fmt::Debug for Image {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        f.debug_struct("Image")
            .field("data", &format_args!("[{} bytes]", self.data.len()))
            .finish()
    }
}

impl From<Image> for Handle {
    fn from(img: Image) -> Self {
        Handle::from_memory(img.data)
    }
}

impl Image {
    pub async fn from_url(url: impl Into<Cow<'_, str>>) -> Result<Self, reqwest::Error> {
        let data = reqwest::get(url.into().as_ref()).await?.bytes().await?;
        Ok(Self {
            data: data.to_vec(),
        })
    }
}
