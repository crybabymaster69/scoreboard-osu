use color_eyre::Result;
use iced::futures::{
    self,
    channel::mpsc::{self, Receiver, Sender},
    SinkExt, StreamExt,
};
use notify::{Config, RecommendedWatcher, RecursiveMode, Watcher};
use std::path::Path;

pub fn async_watcher(
) -> notify::Result<(RecommendedWatcher, Receiver<notify::Result<notify::Event>>)> {
    let (mut tx, rx) = mpsc::channel(1);

    // Automatically select the best implementation for your platform.
    // You can also access each implementation directly e.g. INotifyWatcher.
    let watcher = RecommendedWatcher::new(
        move |res| {
            futures::executor::block_on(async {
                tx.send(res).await.unwrap();
            })
        },
        Config::default(),
    )?;

    Ok((watcher, rx))
}

pub async fn async_watch<P: AsRef<Path>, M: Clone>(
    path: P,
    mut sender: Sender<M>,
    message: M,
) -> Result<()> {
    let (mut watcher, mut rx) = async_watcher()?;
    watcher.watch(path.as_ref(), RecursiveMode::NonRecursive)?;

    while let Some(res) = rx.next().await {
        match res {
            Ok(_event) => sender.send(message.clone()).await?,
            Err(e) => log::error!("notify/watch error: {:?}", e),
        }
    }

    Ok(())
}
