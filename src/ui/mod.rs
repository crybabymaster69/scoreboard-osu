mod filedialog;
mod filewatcher;
mod images;
mod style;

use std::{
    collections::{HashMap, HashSet},
    path::Path,
};

use iced::{
    alignment::{Horizontal, Vertical},
    executor,
    futures::future,
    subscription, theme,
    widget::{button, column, container, image, row, scrollable, text, tooltip},
    Application, Command, Element, Length, Subscription, Theme, Color,
};
use scoreboard_osu::{
    config::Config,
    data::{
        beatmap_exts::{BeatmapExt, ReplayExt},
        recent::{recent_plays, RecentPlay},
        Data, DataError,
    },
    util,
};

use self::style::score_theme;
use self::{filewatcher::async_watch, images::Image};

type Md5 = String;

#[derive(Debug, Default)]
pub struct Ui {
    state: State,
    thumbnail_cache: HashMap<Md5, images::Image>,
    recently_copied: bool,
}

#[derive(Debug, Default)]
pub enum State {
    #[default]
    Loading,
    PromptOsuHome {
        error: DataError,
    },
    Scoreboard {
        plays: Vec<RecentPlay>,
    },
}

#[derive(Debug)]
pub enum Message {
    LoadedScores(Result<Data, DataError>),
    OpenFileDialog,
    FileDialogResult(Option<String>),
    CopyText(String),
    CopyTextTimerExpired,
    OpenUrl(String),
    LoadedThumbnail(Md5, Result<images::Image, reqwest::Error>),
    DbChanged,
}

/// Can't derive it for everybody
impl Clone for Message {
    fn clone(&self) -> Self {
        match self {
            Self::LoadedScores(x) => Self::LoadedScores(x.clone()),
            Self::OpenFileDialog => Self::OpenFileDialog,
            Self::FileDialogResult(x) => Self::FileDialogResult(x.clone()),
            Self::CopyText(x) => Self::CopyText(x.clone()),
            Self::CopyTextTimerExpired => Self::CopyTextTimerExpired,
            Self::OpenUrl(url) => Self::OpenUrl(url.clone()),
            Self::LoadedThumbnail(x, y) => panic!("Can't clone LoadedThumbnail({:?}, {:?})", x, y),
            Self::DbChanged => Self::DbChanged,
        }
    }
}

impl Ui {
    fn view_loading(&self) -> Element<Message> {
        let header = container(column![
            text("Loading...")
                .size(50)
                .width(Length::Fill)
                .horizontal_alignment(Horizontal::Center),
            text(format!(
                "Your osu! home directory is: {}",
                Config::global().osu_home
            ))
            .width(Length::Fill)
            .horizontal_alignment(Horizontal::Center)
        ])
        .padding(30)
        .width(Length::Fill)
        .style(style::HeaderStyle);

        container(header)
            .center_x()
            .center_y()
            .width(Length::Fill)
            .height(Length::Fill)
            .into()
    }

    fn view_prompt_osu_home(&self, error: &DataError) -> Element<Message> {
        let message = if Config::global().osu_home.is_empty() {
            "Your osu! home directory is not configured".to_string()
        } else {
            format!("Couldn't load your scores because {}", error)
        };

        container(
            column![
                text(message)
                    .size(30)
                    .width(Length::Fill)
                    .horizontal_alignment(Horizontal::Center),
                text("Please choose your osu! home directory below (That's the folder that contains osu!.exe)")
                    .size(20)
                    .width(Length::Fill)
                    .horizontal_alignment(Horizontal::Center),
                container(button("Choose").on_press(Message::OpenFileDialog))
                    .width(Length::Fill)
                    .center_x(),
            ]
            .spacing(15)
        )
        .center_x()
        .center_y()
        .width(Length::Fill)
        .height(Length::Fill)
        .into()
    }

    fn view_scoreboard(&self, plays: &[RecentPlay]) -> Element<Message> {
        let mut body = column![container(
            text("Recent plays")
                .size(30)
                .width(Length::Fill)
                .horizontal_alignment(Horizontal::Center)
        )
        .width(Length::Fill)
        .padding(30)
        .style(style::HeaderStyle),];

        const HEIGHT: u16 = 108; // don't ask why
        let mut items = column![];
        for play in plays {
            let str_scores: Vec<_> = play.replays.iter().map(|r| r.score.to_string()).collect();
            let comma_separated_scores = str_scores.join(", ");

            let acc: f32 = play.replays.iter().map(|r| r.accuracy()).sum();
            let acc = 100.0 * acc / play.replays.len() as f32;

            let scores = row(play
                .replays
                .iter()
                .enumerate()
                .map(|(i, r)| {
                    let score = util::put_dots_in_the_middle_of_a_number(r.score);
                    let t = if i == 0 {
                        format!("{}", score)
                    } else {
                        format!(", {}", score)
                    };
                    text(t)
                        .vertical_alignment(Vertical::Center)
                        .style(score_theme(r.score))
                        .into()
                })
                .collect::<Vec<_>>());

            let scores = scores.push(text(format!(" ({:.2}% acc)", acc)).style(
                theme::Text::Color(Color::from_rgba(1.0, 1.0, 1.0, 0.15)),
            ));

            let tooltip_text = if self.recently_copied {
                "[copied]"
            } else {
                "click to copy"
            };
            let scores = tooltip(scores, tooltip_text, tooltip::Position::Bottom)
                .padding(7)
                .gap(5)
                .snap_within_viewport(false)
                .style(style::TooltipStyle);

            let mut parts = row![];

            if let Some(hash) = &play.beatmap.hash {
                if let Some(thumb) = self.thumbnail_cache.get(hash) {
                    parts = parts.push(image(thumb.clone()).width(HEIGHT).height(HEIGHT));
                }
            }

            let title_button =
                button(text(play.beatmap.title()).vertical_alignment(Vertical::Center))
                    .on_press(Message::OpenUrl(play.beatmap.url()))
                    .padding(0)
                    .width(Length::Fill)
                    .style(theme::Button::Text);

            let scores_button = button(scores)
                .style(theme::Button::Text)
                .padding(0)
                .on_press(Message::CopyText(comma_separated_scores));

            parts = parts.push(
                container(column![title_button, scores_button].padding(20).spacing(10))
                    .height(HEIGHT)
                    .center_y(),
            );
            items = items.push(parts);
        }

        body = body.push(container(items).padding(30));
        scrollable(body).into()
    }

    /// Creates the command to load the thumbnails for the given plays
    fn load_thumbnails_command(&mut self, plays: &[RecentPlay]) -> Command<Message> {
        self.clear_unused_thumbnails(plays);
        Command::batch(plays.iter().flat_map(|play| {
            if let Some(hash) = &play.beatmap.hash {
                if !self.thumbnail_cache.contains_key(hash) {
                    let hash = hash.clone();
                    let url = play.beatmap.cover_url();
                    return Some(Command::perform(Image::from_url(url), move |result| {
                        Message::LoadedThumbnail(hash, result)
                    }));
                }
            }
            None
        }))
    }

    /// Deletes the thumbnails that are not used in any of the `plays`
    fn clear_unused_thumbnails(&mut self, plays: &[RecentPlay]) {
        let used_hashes: HashSet<_> = plays
            .iter()
            .filter_map(|play| play.beatmap.hash.clone())
            .collect();

        let old_hashes: Vec<_> = self.thumbnail_cache.keys().cloned().collect();
        for hash in old_hashes {
            if !used_hashes.contains(&hash) {
                self.thumbnail_cache.remove(&hash);
            }
        }
    }

    /// Creates the command to load the scores from the database
    fn load_db_command(&self) -> Command<Message> {
        Command::perform(
            Data::from_dir(Config::global().osu_home.clone()),
            Message::LoadedScores,
        )
    }
}

impl Application for Ui {
    type Executor = executor::Default;
    type Flags = ();
    type Message = Message;
    type Theme = Theme;

    fn new(_flags: ()) -> (Ui, Command<Self::Message>) {
        (
            Ui::default(),
            Command::perform(
                Data::from_dir(Config::global().osu_home.clone()),
                Message::LoadedScores,
            ),
        )
    }

    fn title(&self) -> String {
        String::from("scoreboard-osu")
    }

    fn update(&mut self, msg: Self::Message) -> Command<Self::Message> {
        if let OpenFileDialog = &msg {
            return Command::perform(filedialog::folder(), Message::FileDialogResult);
        };

        if let LoadedThumbnail(hash, Ok(image)) = &msg {
            self.thumbnail_cache.insert(hash.clone(), image.clone());
            return Command::none();
        }

        if let CopyText(text) = msg {
            // Write to the clipboard and wait 0.3s before removing the "copied" text
            self.recently_copied = true;
            return Command::batch([
                iced::clipboard::write(text),
                Command::perform(
                    async {
                        tokio::time::sleep(std::time::Duration::from_millis(300)).await;
                        Message::CopyTextTimerExpired
                    },
                    |msg| msg,
                ),
            ]);
        }

        if let CopyTextTimerExpired = msg {
            self.recently_copied = false;
        }

        if let OpenUrl(url) = msg {
            webbrowser::open(&url).unwrap();
            return Command::none();
        }

        use Message::*;
        match self.state {
            State::Loading => match msg {
                LoadedScores(Ok(data)) => {
                    // The dbs changed while the data was loading, we have to reload it...
                    if data.outdated(&Config::global().osu_home) {
                        self.state = State::Loading;
                        return self.load_db_command();
                    }

                    let plays = recent_plays(&data);
                    let cmd = self.load_thumbnails_command(&plays);
                    // TODO: maybe save the data one day
                    // self.data = Some(data);
                    self.state = State::Scoreboard { plays };
                    cmd
                }
                LoadedScores(Err(e)) => {
                    self.state = State::PromptOsuHome { error: e };
                    Command::none()
                }
                _ => Command::none(),
            },
            State::PromptOsuHome { error: _ } => match msg {
                FileDialogResult(Some(osu_home)) => {
                    // Replace osu_home in config
                    Config::global().osu_home = osu_home.clone();
                    Config::global().save();

                    self.state = State::Loading;
                    self.load_db_command()
                }
                FileDialogResult(None) => Command::none(),
                _ => Command::none(),
            },
            State::Scoreboard { .. } => match msg {
                DbChanged => {
                    self.state = State::Loading;
                    self.load_db_command()
                }
                _ => Command::none(),
            },
        }
    }

    fn view(&self) -> Element<Self::Message> {
        match &self.state {
            State::Loading => self.view_loading(),
            State::PromptOsuHome { error } => self.view_prompt_osu_home(error),
            State::Scoreboard { plays } => self.view_scoreboard(plays),
        }
    }

    fn subscription(&self) -> Subscription<Self::Message> {
        // Notify when the osu! database changes by sending Message::DbChanged
        subscription::channel("ui/notify-db-changes", 1, |output| async move {
            let home = Config::global().osu_home.clone();
            let home = Path::new(&home);
            let create_watcher = |path| async_watch(path, output.clone(), Message::DbChanged);

            let osudb_watcher = create_watcher(home.join("osu!.db"));
            let scoresdb_watcher = create_watcher(home.join("scores.db"));
            let _ = future::join(osudb_watcher, scoresdb_watcher).await;
            future::pending().await
        })
    }

    fn theme(&self) -> Theme {
        Theme::Dark
    }
}
