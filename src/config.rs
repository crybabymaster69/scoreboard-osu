use lazy_static::lazy_static;
use serde_json::json;
use std::sync::{Mutex, MutexGuard};

#[derive(Debug, Default)]
pub struct Config {
    pub osu_home: String,
}

// Mutable global state :D
lazy_static! {
    static ref INSTANCE: Mutex<Config> = Mutex::new(Config::default());
}

impl Config {
    pub fn global() -> MutexGuard<'static, Config> {
        INSTANCE.lock().unwrap()
    }

    pub fn load(&mut self) {
        let file = dirs::config_dir().unwrap().join("scoreboard-osu.json");
        let content = match std::fs::read_to_string(file) {
            Ok(content) => content,
            Err(_) => return,
        };
        let data = match serde_json::from_str::<serde_json::Value>(&content) {
            Ok(data) => data,
            Err(_) => return,
        };

        if let Some(osu_home) = data["osu_home"].as_str() {
            self.osu_home = osu_home.to_string();
        }
    }

    pub fn save(&self) {
        let data = json!({
            "osu_home": self.osu_home,
        });

        let file = dirs::config_dir().unwrap().join("scoreboard-osu.json");
        std::fs::write(file, data.to_string()).unwrap();
    }
}
