# scoreboard-osu

A thing that shows your recent osu! scores, mostly for tournament-related convenience.

# Download
[Check the releases](https://gitlab.com/crybabymaster69/scoreboard-osu/-/releases)

# How do I run this?
Open `scoreboard-osu.exe` if you're on Windows, or `scoreboard-osu` if you use Linux.
