#!/bin/bash

set -x

# Build Rust source
cargo build --release --target x86_64-unknown-linux-gnu
cargo build --release --target x86_64-pc-windows-gnu

# Make relese zip  
rm -f scoreboard-osu.zip
7z a scoreboard-osu.zip                                       \
    ./target/x86_64-pc-windows-gnu/release/scoreboard-osu.exe \
    ./target/x86_64-unknown-linux-gnu/release/scoreboard-osu  \
    README.md                                                 \

